# frozen_string_literal: true

module Slack
  class Messenger
    VERSION = "2.3.5".freeze # rubocop:disable Style/RedundantFreeze
  end
end
